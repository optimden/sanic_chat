from marshmallow import Schema, EXCLUDE, ValidationError
from sanic.exceptions import SanicException

from api.exceptions import ApiRequestValidationException, ApiResponseValidationException


# базовые классы DTO
class RequestDTO:
    __schema__: Schema

    def __init__(self, data: dict):
        try:
            valid_data = self.__schema__(unknown=EXCLUDE).load(data)
        except ValidationError as error:
            raise ApiRequestValidationException(error.messages)
        else:
            self._import(valid_data)
        
    def _import(self, data: dict):
        for key, value in data.items():
            self.set(key, value)
    
    def set(self, key, value):
        setattr(self, key, value)


class ResponseDTO:
    __schema__: Schema

    def __init__(self, obj, many: bool = False):

        if many:
            properties = [self.parse_object(model) for model in obj]
        else:
            properties = self.parse_object(obj)

        try:
            self._data = self.__schema__(unknown=EXCLUDE, many=many).load(properties)
        except ValidationError as error:
            raise ApiResponseValidationException(error.messages)
    

    @staticmethod
    def parse_object(model: object) -> dict:
        return {
            prop: value
            for prop in dir(model)
            if not prop.startswith('_')
                and not prop.endswith('_')
                and not callable(value := getattr(model, prop))
        }


    def dump(self) -> dict:
        return self._data

