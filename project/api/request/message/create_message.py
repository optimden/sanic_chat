from marshmallow import Schema, fields

from api.base import RequestDTO


class RequestCreateMessageDTOSchema(Schema):
    message = fields.Str(required=True, allow_none=False)
    recipient = fields.Str(required=True, allow_none=False)


class RequestCreateMessageDTO(RequestDTO, RequestCreateMessageDTOSchema):
    __schema__ = RequestCreateMessageDTOSchema