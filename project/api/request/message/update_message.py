from marshmallow import Schema, fields

from api.base import RequestDTO


class RequestUpdateMessageDTOSchema(Schema):
    message = fields.String(required=True, allow_none=False)


class RequestUpdateMessageDTO(RequestDTO, RequestUpdateMessageDTOSchema):
    __schema__ = RequestUpdateMessageDTOSchema
