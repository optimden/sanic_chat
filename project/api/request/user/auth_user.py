from marshmallow import Schema, fields

from api.base import RequestDTO


class RequestAuthUserDTOSchema(Schema):
    login = fields.Str(required=True, allow_none=False)
    password = fields.Str(required=True, allow_none=False)


class RequestAuthUserDTO(RequestDTO, RequestAuthUserDTOSchema):
    __schema__ = RequestAuthUserDTOSchema