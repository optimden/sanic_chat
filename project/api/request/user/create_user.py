from marshmallow import Schema, fields

from api.base import RequestDTO


class RequestCreateUserDTOSchema(Schema):
    login = fields.Str(required=True, allow_none=False)
    password = fields.Str(required=True, allow_none=False)
    first_name = fields.Str(required=True, allow_none=False)
    last_name = fields.Str(required=True, allow_none=False)


class RequestCreateUserDTO(RequestDTO, RequestCreateUserDTOSchema):
    __schema__ = RequestCreateUserDTOSchema