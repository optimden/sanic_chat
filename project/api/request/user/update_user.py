from marshmallow import Schema, fields

from api.base import RequestDTO


class RequestUpdateUserDTOSchema(Schema):
    password = fields.Str()
    first_name = fields.Str()
    last_name = fields.Str()


class RequestUpdateUserDTO(RequestDTO, RequestUpdateUserDTOSchema):
    fields: list
    __schema__ = RequestUpdateUserDTOSchema

    def __init__(self, *args, **kwargs):
        self.fields = []
        super(RequestUpdateUserDTO, self).__init__(*args, **kwargs)
    
    def set(self, key, value):
        self.fields.append(key)
        super(RequestUpdateUserDTO, self).set(key, value)