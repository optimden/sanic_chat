from datetime import datetime

from marshmallow import Schema, fields, pre_load, post_load

from api.base import ResponseDTO


class ResponseMessageDTOSchema(Schema):
    id = fields.Int(required=True, allow_none=False)
    message = fields.Str(required=True, allow_none=False)
    sender_id = fields.Int(required=True, allow_none=False)
    receiver_id = fields.Int(required=True, allow_none=False)
    created_at = fields.DateTime(required=True, allow_none=False)
    updated_at = fields.DateTime(required=True, allow_none=False)

    @pre_load
    @post_load
    def deserialize_datetime(self, data: dict, **kwargs) -> dict:
        if 'created_at' in data:
            data['created_at'] = self.convert_datetime_to_iso(data['created_at'])
        if 'updated_at' in data:
            data['updated_at'] = self.convert_datetime_to_iso(data['updated_at'])
        return data

    @staticmethod
    def convert_datetime_to_iso(dt):
        if isinstance(dt, datetime):
            return dt.isoformat()
        return dt


class ResponseMessageDTO(ResponseDTO, ResponseMessageDTOSchema):
    __schema__ = ResponseMessageDTOSchema
