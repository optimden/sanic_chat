from db.config import PostgreSQLConfig, RedisConfig
from transport.sanic.config import SanicConfig


class ApplicationConfig:
    sanic: SanicConfig
    database: PostgreSQLConfig
    redis: RedisConfig

    def __init__(self):
        self.sanic = SanicConfig()
        self.database = PostgreSQLConfig()
        self.redis = RedisConfig()