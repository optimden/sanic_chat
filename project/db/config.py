import os
from dotenv import load_dotenv


load_dotenv()


class PostgreSQLConfig:
    name = os.getenv('POSTGRES_DB', 'default')
    user = os.getenv('POSTGRES_USER', 'admin')
    password = os.getenv('POSTGRES_PASSWORD', 'admin')
    host = os.getenv('POSTGRES_HOST', 'postgres')
    port = os.getenv('POSTGRES_PORT', 5432)
    url = rf'postgresql://{user}:{password}@{host}:{port}/{name}'


class RedisConfig:
    host=os.getenv("REDIS_HOST", "redis")
    port=int(os.getenv("REDIS_PORT", "6379"))
    db=int(os.getenv("REDIS_DB", "0"))
    decode_responses=os.getenv("REDIS_DECODE_RESPONSE", True)