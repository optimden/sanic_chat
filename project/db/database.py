from logging import getLogger
from typing import List

from sqlalchemy.engine import Engine
from sqlalchemy.exc import DataError, IntegrityError
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy.orm.exc import NoResultFound

from db.exceptions import (
    DBDataException,
    DBIntegrityException,
    DBUserNotFoundException,
    DBMessageNotFoundException
)
from db.models import User, Message


class DBSession:
    _session: Session

    def __init__(self, session: Session):
        self._session = session
        self._logger = getLogger("chat_logger")

    def query(self, *args, **kwargs):
        return self._session.query(*args, **kwargs)

    def add_model(self, instance: object, *args, **kwrags):
        try:
            self._session.add(instance)
        except DataError as error:
            raise DBDataException(error)
        except IntegrityError as error:
            raise DBIntegrityException(error)
    
    def commit_session(self):
        try:
            self._session.commit()
        except (TypeError, DataError) as error:
            raise DBDataException(error)
        except IntegrityError as error:
            raise DBIntegrityException(error)
    
    def rollback(self):
        self._session.rollback()
    
    def close_session(self):
        self._session.close()
        
    def find_user_by_id(self, user_id: int) -> User:
        try:
            user_query = self.query(User).filter(
                User.id == user_id,
                User.is_deleted == False
                )
            return user_query.one()
        except NoResultFound as error:
            raise DBUserNotFoundException
        else:
            self._logger.debug(str(user_query))

    def find_user_by_login(self, login: str) -> User:
        try:
            user_query = self.query(User).filter(
                User.login == login,
                User.is_deleted == False
                )
            return user_query.one()
        except NoResultFound as error:
            raise DBUserNotFoundException
        else:
            self._logger.debug(str(user_query))
    
    def get_messages(self, user_id: int) -> List[Message]:
        messages_query = self.query(Message).filter(
                            Message.receiver_id == user_id,
                            Message.is_deleted == False
                        ).order_by(Message.created_at)
        self._logger.debug(str(messages_query))
        return messages_query.all()

    def get_message(self, msg_id: int) -> Message:
        try:
            message_query = self.query(Message).filter(
                Message.id == msg_id,
                Message.is_deleted == False
                )
            return message_query.one()
        except NoResultFound:
            raise DBMessageNotFoundException
        else:
            self._logger.debug(str(message_query))


class DataBase:
    connection: Engine
    session_factory: sessionmaker
    _test_query = 'SELECT 1'

    def __init__(self, connection: Engine):
        self.connection = connection
        self.session_factory = sessionmaker(bind = self.connection)
    
    def check_connection(self):
        self.connection.execute(self._test_query).fetchone()

    def make_session(self) -> DBSession:
        session = self.session_factory()
        return DBSession(session)
