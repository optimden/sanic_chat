class DBDataException(Exception):
    pass


class DBIntegrityException(Exception):
    pass


class DBUserNotFoundException(Exception):
    pass


class DBUserExistsException(Exception):
    pass

class DBMessageNotFoundException(Exception):
    pass

class CommonRedisException(Exception):
    pass