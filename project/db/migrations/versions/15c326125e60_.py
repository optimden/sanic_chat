"""empty message

Revision ID: 15c326125e60
Revises: b4543f2f257c
Create Date: 2021-01-27 21:36:24.942677

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '15c326125e60'
down_revision = 'b4543f2f257c'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
