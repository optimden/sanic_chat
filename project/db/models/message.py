from sqlalchemy import Column, Integer, ForeignKey, String, BOOLEAN
from sqlalchemy.orm import relationship

from .base import BaseModel


class Message(BaseModel):

    __tablename__ = 'messages'

    message = Column(
        String(200),
        nullable=False,
    )

    sender_id = Column(
        Integer(),
        ForeignKey('users.id', ondelete="CASCADE"),
        nullable=False,
    )

    receiver_id = Column(
        Integer(),
        ForeignKey('users.id', ondelete="CASCADE"),
        nullable=False,
    )

    is_deleted = Column(
        BOOLEAN(),
        nullable=False,
        default=False,
    )

    sender = relationship("User", foreign_keys=[sender_id])
    receiver = relationship("User", foreign_keys=[receiver_id])