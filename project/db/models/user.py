from sqlalchemy import Column, ForeignKey, VARCHAR, BOOLEAN, LargeBinary
from sqlalchemy.orm import relationship

from .base import BaseModel


class User(BaseModel):

    __tablename__ = 'users'

    first_name = Column(VARCHAR(50), nullable=False)

    last_name = Column(VARCHAR(50), nullable=False)

    password = Column(LargeBinary(), nullable=False)

    login = Column(VARCHAR(20), nullable=False, unique=True)

    is_deleted = Column(BOOLEAN(), nullable=False, default=False)
