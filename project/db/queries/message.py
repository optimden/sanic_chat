from datetime import datetime
from typing import List

from sqlalchemy.orm.exc import NoResultFound

from api.request.message.create_message import RequestCreateMessageDTO
from db.database import DBSession
from db.exceptions import DBMessageNotFoundException
from db.models import Message
from transport.sanic.exceptions import SanicAccessException, SanicMessageIsDeletedException


def get_messages(session: DBSession, user_id: int) -> List[Message]:
    messages = session.get_messages(user_id)
    return messages


def create_message(session: DBSession, dto: RequestCreateMessageDTO, sender_id: int, receiver_id: int) -> Message:
    message = Message(
        message=dto.message,
        sender_id=sender_id,
        receiver_id=receiver_id
        )
        
    session.add_model(message)

    return message


def get_message(session: DBSession, msg_id: int) -> Message:
    try:
        return session.get_message(msg_id)
    except NoResultFound:
        raise DBMessageNotFoundException


def delete_message(session: DBSession, msg_id: int, user_id: int) -> Message:
    message = session.get_message(msg_id)
    message.is_deleted = True
    return message
    
    
def update_message(session: DBSession, request_model, msg_id: int, user_id: int) -> Message:
    message = session.get_message(msg_id)
    message.message = request_model.message
    message.updated_at = datetime.utcnow()
    return message


