from api.request.user.create_user import RequestCreateUserDTO
from api.request.user.update_user import RequestUpdateUserDTO

from db.database import DBSession
from db.exceptions import DBUserNotFoundException, DBUserExistsException
from db.models import User

from transport.sanic.exceptions import SanicUserNotFoundException, SanicUserIsDeletedException


def create_user(session: DBSession, dto: RequestCreateUserDTO, password: bytes) -> User:

    try:
        existed_user = session.find_user_by_login(dto.login)
    except DBUserNotFoundException:
        pass
    else:
        raise DBUserExistsException 

    user = User(
        first_name = dto.first_name,
        last_name = dto.last_name,
        login = dto.login,
        password = password
    )
    session.add_model(user)

    return user


def update_user(session: DBSession, dto: RequestUpdateUserDTO, user_id: int) -> User:
    user = session.find_user_by_id(user_id)
    for parameter in dto.fields:
        if hasattr(user, parameter):
            setattr(user, parameter, getattr(dto, parameter))
    return user


def delete_user(session: DBSession, user_id: int) -> User:
    try:
        user = session.find_user_by_id(user_id)
    except DBUserNotFoundException as error:
        raise DBUserNotFoundException(error)
    else:
        user.is_deleted = True
        return user