import redis
from redis.exceptions import RedisError

from db.exceptions import CommonRedisException


class Redis:

    def __init__(self, host: str, port: int, db: int = 0, decode_responses: bool = True):
        self._redis = redis.StrictRedis(
            host=host, 
            port=port, 
            db=db, 
            decode_responses=decode_responses
        )
    
    def unlink(self, key: str):
        try:
            self._redis.unlink(key)
        except RedisError as error:
            raise CommonRedisException(error)

    def set_(self, key: str, value):
        try:
            self._redis.set(key, value)
        except RedisError as error:
            raise CommonRedisException(error)
    
    def get(self, key: str):
        self._redis.get(key)