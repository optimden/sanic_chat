import logging
from sqlalchemy import create_engine

from configs.config import ApplicationConfig
from context import Context
from db.database import DataBase
from db.redis import Redis
from logger.config import LoggerConfig


def init_db(config: ApplicationConfig, context: Context):
    engine = create_engine(
        config.database.url,
        pool_pre_ping = True,
    )

    database = DataBase(connection = engine)

    database.check_connection()

    context.set('database', database)


def init_redis(config: ApplicationConfig, context: Context):
    redis = Redis(
        host=config.redis.host,
        port=config.redis.port,
        db=config.redis.db,
        decode_responses=config.redis.decode_responses
    )
    context.set('redis', redis)


def create_logger(context: Context):
    log_config = LoggerConfig()
    logging.config.dictConfig(log_config.config)
    logger = logging.getLogger("chat_logger")
    context.set('logger', logger)
    

