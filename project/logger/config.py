import logging
from copy import copy

from sanic.log import LOGGING_CONFIG_DEFAULTS


class LoggerConfig:

    log_config = {
        "version": 1,
        "handlers": {
            "stream_handler": {
                "class": "logging.StreamHandler",
                "formatter": "my_formatter"
            }
        },
        "loggers": {
            "chat_logger": {
                "handlers": ["stream_handler",],
                "level": "INFO"
            }
        },
        "formatters": {
            "my_formatter": {
                "format": "[%(asctime)s] - [%(name)s] - [%(levelname)s] - (%(filename)s).%(funcName)s(%(lineno)d) - %(message)s"
                }
        }
    }

    def __init__(self):
        self.config = copy(LOGGING_CONFIG_DEFAULTS)
        for key, value in self.config.items():
            if isinstance(value, dict):
                self.config[key].update(LoggerConfig.log_config[key])
        
