import os
from dotenv import load_dotenv


load_dotenv()

class SanicConfig:
    host = os.getenv('SANIC_HOST', '127.0.0.1')
    port = int(os.getenv('SANIC_PORT', 8000))
    workers = int(os.getenv('SANIC_WORKERS', 1))
    debug = bool(os.getenv('SANIC_DEBUG', False))
    
    