from http import HTTPStatus
from typing import Iterable

from sanic.request import Request
from sanic.response import BaseHTTPResponse, json

from context import Context
from db.exceptions import DBUserNotFoundException
from transport.sanic.exceptions import (
    SanicException,
    SanicAuthException,
    SanicUserNotFoundException,
)
from utils.auth.exceptions import DecodeTokenException
from utils.auth.token import read_token


class Endpoint:

    async def __call__(self, request: Request, *args, **kwargs):
        if self.auth_required:
            try:
                token = await self.check_auth(request, *args, **kwargs)
            except SanicException as error:
                return await self.create_json_response(status=error.status_code)
            kwargs.update(token)
        return await self.handler(request, *args, **kwargs)

    def __init__(self, context: Context, uri: str, methods: Iterable, auth_required: bool = False, *args, **kwargs):
        self.context = context
        self.uri = uri
        self.methods = methods
        self.auth_required = auth_required
        self.session = self.context.database.make_session()
        self.logger = self.context.logger

        self.__name__ = self.__class__.__name__
    
    @staticmethod
    async def create_json_response(
        status: int = 200, body: dict = None, error_code: int = None, message: str = None, cookies: list = None, cookies_reset: str = None
    ) -> BaseHTTPResponse:
        
        if body is None:
            body = {
                'message': message or HTTPStatus(status).phrase,
                'error_code': error_code or status,
            }
        
        response = json(body=body, status=status)

        if cookies is not None:
            for cookie in cookies:
                response.cookies[cookie["name"]] = cookie["value"]
                response.cookies[cookie["name"]].update(cookie["params"])

        if cookies_reset is not None:
            del response.cookies[cookies_reset]

        return response
    
    @staticmethod
    def get_request_json(request: Request) -> dict:
        if 'application/json' in request.content_type and request.json is not None:
            return dict(request.json)
        return {}
    
    @staticmethod
    def get_request_headers(request: Request) -> dict:
        return {
            header: value
            for header, value in request.headers.items()
            if header.lower().startswith('x-')
        }
    
    @staticmethod
    def get_auth_token(request: Request) -> dict:
        token = request.headers.get("Authorization")

        return read_token(token)
    
    async def check_auth(self, request: Request, *args, **kwargs) -> dict:
        try:
            token = {
                "token": self.get_auth_token(request)
                }
        except DecodeTokenException as error:
            raise SanicAuthException(str(error))

        user_id = token.get("token").get("user_id")

        try:
            user = self.session.find_user_by_id(user_id)
        except DBUserNotFoundException as error:
            raise SanicUserNotFoundException(str(error))

        return token

    async def handler(self, request: Request, *args, **kwargs) -> BaseHTTPResponse:
        body = {}

        body.update(self.get_request_json(request))
        body.update(self.get_request_headers(request))

        return await self._identify_method(request, body, *args, **kwargs)

    async def _identify_method(self, request: Request, body: dict, *args, **kwargs) -> BaseHTTPResponse:
        method = request.method.lower()
        func_name = f'method_{method}'
        
        if hasattr(self, func_name):
            func = getattr(self, func_name)
            return await func(request, body, *args, **kwargs)
        return await self.method_not_impl(method=method)
    
    async def method_not_impl(self, method: str):
        return await self.create_json_response(
            status=501, message=f'Method {method.upper()} not implemented.' 
        )
    
    async def method_get(self, request: Request, body: dict, *args, **kwargs) -> BaseHTTPResponse:
        return await self.method_not_impl(method='GET')
    
    async def method_post(self, request: Request, body: dict, *args, **kwargs) -> BaseHTTPResponse:
        return await self.method_not_impl(method='POST')

    async def method_patch(self, request: Request, body: dict, *args, **kwargs) -> BaseHTTPResponse:
        return await self.method_not_impl(method='PATCH')

    async def method_delete(self, request: Request, body: dict, *args, **kwargs) -> BaseHTTPResponse:
        return await self.method_not_impl(method='DELETE')
    

    