from sanic.request import Request
from sanic.response import BaseHTTPResponse

from transport.sanic.endpoint import Endpoint
from transport.sanic.exceptions import SanicException


class BaseEndpoint(Endpoint):

    async def _identify_method(self, request: Request, body: dict, *args, **kwargs) -> BaseHTTPResponse:

        try:
            return await super()._identify_method(request, body, *args, **kwargs)
        except SanicException as error:
            return await self.create_json_response(status=error.status_code, message=str(error))





