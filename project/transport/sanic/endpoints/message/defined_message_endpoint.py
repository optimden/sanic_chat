from sanic.request import Request
from sanic.response import BaseHTTPResponse

from api.request.message.update_message import RequestUpdateMessageDTO
from api.response.message.message import ResponseMessageDTO

from db.exceptions import (
    DBMessageNotFoundException,
    DBDataException,
    DBIntegrityException
)
from db.queries.message import delete_message, update_message

from transport.sanic.endpoints.base import BaseEndpoint
from transport.sanic.exceptions import (
    SanicMessageNotFoundException,
    SanicDBException,
    SanicAccessException,
    SanicMessageIsDeletedException
    )


class DefinedMessageEndpoint(BaseEndpoint):
    
    async def method_get(self, request: Request, body: dict, msg_id: int, token: dict, *args, **kwargs) -> BaseHTTPResponse:
        user_id = token.get("user_id")

        self.logger.info(f"Request: GET/msg/{msg_id} by user_{user_id}")
        
        try:
            message = self.session.get_message(msg_id)
        except DBMessageNotFoundException as error:
            raise SanicMessageNotFoundException(str(error))

        if message.receiver_id != user_id:
            raise SanicAccessException("Forbidden.")

        response_model = ResponseMessageDTO(message)

        return await self.create_json_response(status=200, body=response_model.dump())


    async def method_delete(self, request: Request, body: dict, msg_id: int, token: dict, *args, **kwargs) -> BaseHTTPResponse:

        user_id = token.get("user_id")

        self.logger.info(f"Request: DELETE/msg/{msg_id} by user_{user_id}")

        try:
            message = delete_message(self.session, msg_id, user_id)
        except DBMessageNotFoundException as error:
            raise SanicMessageNotFoundException(str(error))

        if message.sender_id != user_id:
            raise SanicAccessException("Forbidden.")

        try:
            self.session.commit_session()
        except (DBDataException, DBIntegrityException) as error:
            self.session.rollback()
            self.logger.exception("Exception was occured.")
            raise SanicDBException(str(error))

        return await self.create_json_response(status=200)
    

    async def method_patch(self, request: Request, body: dict, msg_id: int, token: dict, *args, **kwargs) -> BaseHTTPResponse:
        user_id = token.get("user_id")

        self.logger.info(f"Request: PATCH/msg/{msg_id} by user_{user_id}")
        
        request_model = RequestUpdateMessageDTO(body)

        try:
            message = update_message(self.session, request_model, msg_id, user_id)
        except DBMessageNotFoundException as error:
            raise SanicMessageNotFoundException(str(error))
        
        if message.sender_id != user_id:
            raise SanicAccessException("Forbidden.")

        try:
            self.session.commit_session()
        except (DBDataException, DBIntegrityException) as error:
            self.session.rollback()
            self.logger.exception("Exception was occured.")
            raise SanicDBException(str(error))

        response_model = ResponseMessageDTO(message)

        return await self.create_json_response(status=200, body=response_model.dump())

            