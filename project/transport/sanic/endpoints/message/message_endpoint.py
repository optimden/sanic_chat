from sanic.request import Request
from sanic.response import BaseHTTPResponse

from api.request.message.create_message import RequestCreateMessageDTO
from api.response.message.message import ResponseMessageDTO

from db.exceptions import (
    DBDataException, 
    DBIntegrityException, 
    DBUserNotFoundException, 
    DBMessageNotFoundException,
)
from db.queries.message import create_message, delete_message
from db.models.user import User
from db.models.message import Message

from transport.sanic.endpoints.base import BaseEndpoint
from transport.sanic.exceptions import (
    SanicDBException,
    SanicUserNotFoundException,
    SanicMessageNotFoundException,
)


class MessageEndpoint(BaseEndpoint):

    async def method_get(self, request: Request, body: dict, token: dict, *args, **kwargs) -> BaseHTTPResponse:
        user_id = token.get("user_id")

        self.logger.info(f"Request: GET/msg by user_{user_id}")

        messages = self.session.get_messages(user_id)

        response_model = ResponseMessageDTO(messages, many=True)

        return await self.create_json_response(body=response_model.dump())


    async def method_post(self, request: Request, body: dict, token: dict, *args, **kwargs) -> BaseHTTPResponse:
        """ Создает новое сообщение от пользователя с user_id (отправителя) 
        пользователя с заданным login (получателю). """
        user_id = token.get("user_id")

        self.logger.info(f"Request: POST/msg by user_{user_id}")
        
        request_model = RequestCreateMessageDTO(body)

        try:
            sender = self.session.find_user_by_id(user_id)
        except DBUserNotFoundException as error:
            raise SanicUserNotFoundException(str(error))

        try:
            receiver = self.session.find_user_by_login(request_model.recipient)
        except DBUserNotFoundException as error:
            raise SanicUserNotFoundException(str(error))

        message = create_message(self.session, request_model, user_id, receiver.id)

        try:
            self.session.commit_session()
        except (DBDataException, DBIntegrityException) as error:
            self.session.rollback()
            self.logger.exception("Exception was occured.")
            raise SanicDBException(str(error))

        return await self.create_json_response(status=201)