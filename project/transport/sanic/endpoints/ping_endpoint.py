from sanic.request import Request
from sanic.response import BaseHTTPResponse

from transport.sanic.endpoint import Endpoint


class PingEndpoint(Endpoint):

    async def method_get(self, request: Request, body: dict, *args, **kwargs) -> BaseHTTPResponse:
        response = {"all": "wright"}
        return await self.create_json_response(body=response)


    async def method_post(self, request: Request, body: dict, *args, **kwargs) -> BaseHTTPResponse:
        return await self.create_json_response(body=body)

