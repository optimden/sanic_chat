import datetime

from sanic.request import Request
from sanic.response import BaseHTTPResponse

from api.request.user.auth_user import RequestAuthUserDTO

from db.exceptions import DBUserNotFoundException, CommonRedisException

from transport.sanic.endpoints.base import BaseEndpoint
from transport.sanic.exceptions import (
    SanicUserNotFoundException, 
    SanicRedisException, 
    SanicPasswordHashException,
    SanicTokenException
)

from utils.auth.exceptions import DecodeTokenException
from utils.auth.token import read_token, create_token, set_refresh_token
from utils.hash.exceptions import CheckPasswordHashException
from utils.hash.hash import compare_hash


class AuthenticationEndpoint(BaseEndpoint):
    
    async def method_get(self, request: Request, body: dict, *args, **kwargs) -> BaseHTTPResponse:
        """ Выдаем пользователю новую пару access/refresh токенов. Сохраняем refresh токен в Redis. """

        # 1. Получаем из cookie refresh токен
        current_refresh = request.cookies.get("refresh_token")
        if current_refresh is None:
            return await self.create_json_response(status=400, message="Refresh-token is absent.")

        # 2. Получаем из refresh токена id пользователя
        try:
            token_dict = read_token(current_refresh)
        except DecodeTokenException as error:
            raise SanicTokenException(str(error))

        user_id = token_dict.get("user_id")

        if user_id is None:
            return await self.create_json_response(status=400, message="Incorrect token.")

        # 3. Ищем refresh токен для пользователя с таким id в Redis, сравниваем токены
        saved_refresh = self.context.redis.get(user_id)
        if saved_refresh is None:
            return await self.create_json_response(status=400, message="Refresh-token is absent.")
        if saved_refresh != current_refresh:
            return await self.create_json_response(status=400, message="Incorrect token.")

        # 4. Генерируем новые access и refresh токены, сохраняем последний в Redis
        try:
            cookie = set_refresh_token(self.context.redis, user_id, lifetime=30)
        except CommonRedisException as error:
            self.logger.exception("Exception was occured.")
            raise SanicRedisException(str(error))

        response_body = {
            "Authorization": create_token({"user_id": user_id, "type": "access"}, lifetime=1),
        }

        return await self.create_json_response(status=200, body=response_body, cookies=[cookie,])



    async def method_post(self, request: Request, body: dict, *args, **kwargs) -> BaseHTTPResponse:
        request_model = RequestAuthUserDTO(body)

        try:
            user = self.session.find_user_by_login(request_model.login)
        except DBUserNotFoundException as error:
            raise SanicUserNotFoundException(str(error))

        try:
            compare_hash(request_model.password, user.password)
        except CheckPasswordHashException:
            raise SanicPasswordHashException("Wrong password.")

        response_body = {
            "Authorization": create_token(
                {
                    "user_id": user.id,
                    "type": "access"
                },
                lifetime=1
                )
        }

        try:
            cookie = set_refresh_token(self.context.redis, user.id, lifetime=30)
        except CommonRedisException as error:
            self.logger.exception("Exception was occured.")
            raise SanicTokenException(str(error))

        return await self.create_json_response(body=response_body, cookies=[cookie,])
