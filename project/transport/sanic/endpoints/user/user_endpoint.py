import bcrypt
from sanic.request import Request
from sanic.response import BaseHTTPResponse, json

from api.exceptions import ApiRequestValidationException, ApiResponseValidationException
from api.request.user.create_user import RequestCreateUserDTO
from api.request.user.update_user import RequestUpdateUserDTO
from api.response.user.user import ResponseUserDTO

from db.database import DBSession
from db.exceptions import (
    DBUserNotFoundException,
    DBDataException,
    DBUserExistsException,
    DBIntegrityException,
    CommonRedisException
)
from db.queries.user import create_user, update_user, delete_user
from db.models.user import User

from transport.sanic.endpoints.base import BaseEndpoint
from transport.sanic.exceptions import (
    SanicUserNotFoundException, 
    SanicDBException, 
    SanicRedisException,
    SanicPasswordHashException, 
    SanicUserConflictException,
    SanicUserIsDeletedException,
    SanicTokenException
)

from utils.auth.token import create_token, set_refresh_token
from utils.hash.exceptions import GeneratePasswordHashException
from utils.hash.hash import generate_hash


class UserEndpoint(BaseEndpoint):
    
    async def method_get(self, request: Request, body: dict, token: dict, *args, **kwargs) -> BaseHTTPResponse:
        user_id = token.get("user_id")

        self.logger.info(f"Request: GET/user by user_{user_id}")

        try:
            user = self.session.find_user_by_id(user_id)
        except DBUserNotFoundException as error:
            raise SanicUserNotFoundException(str(error))
        
        response_model = ResponseUserDTO(user)

        return await self.create_json_response(status=200, body=response_model.dump())
            

    async def method_post(self, request: Request, body: dict, *args, **kwargs) -> BaseHTTPResponse:

        self.logger.info(f"Request: POST/user")

        request_model = RequestCreateUserDTO(body)

        try:
            hashed_password = generate_hash(request_model.password)
        except GeneratePasswordHashException as error:
            raise SanicPasswordHashException(str(error))

        try:
            user = create_user(self.session, request_model, hashed_password)
            self.logger.info(f"User '{user.login}' with id: {user.id} was created.")
        except DBUserExistsException as error:
            raise SanicUserConflictException(str(error))

        try: 
            self.session.commit_session()
        except (DBDataException, DBIntegrityException) as error:
            self.session.rollback()
            self.logger.exception("Exception was occured.")
            raise SanicDBException(str(error))

        response_model = ResponseUserDTO(user)

        access_payload = {"user_id": user.id, "type": "access"}
        response = response_model.dump()
        response.update({"Authorization": create_token(access_payload, lifetime=1)})

        try:
            cookie = set_refresh_token(self.context.redis, user.id, lifetime=30)
        except CommonRedisException as error:
            self.logger.exception("Exception was occured.")
            raise SanicTokenException(str(error))

        return await self.create_json_response(status=201, body=response, cookies=[cookie,])


    async def method_patch(self, request: Request, body: dict, token: dict, *args, **kwargs) -> BaseHTTPResponse:

        user_id = token.get("user_id")

        self.logger.info(f"Request: PATCH/user by user_{user_id}")

        request_model = RequestUpdateUserDTO(body)
        
        try:
            user = update_user(self.session, request_model, user_id)
        except DBUserNotFoundException as error:
            raise SanicUserNotFoundException(str(error))

        try:
            self.session.commit_session()
        except (DBDataException, DBIntegrityException) as error:
            self.session.rollback()
            self.logger.exception("Exception was occured.")
            raise SanicDBException(str(error))

        response_model = ResponseUserDTO(user)

        return await self.create_json_response(status=200, body=response_model.dump())
    

    async def method_delete(self, request: Request, body: dict, token: dict, *args, **kwargs) -> BaseHTTPResponse:
        user_id = token.get("user_id")

        self.logger.info(f"Request: DELETE/user by user_{user_id}")

        try:
            user = delete_user(self.session, user_id)
        except DBUserNotFoundException as error:
            raise SanicUserNotFoundException(str(error))
        
        try:
            self.session.commit_session()
        except (DBDataException, DBIntegrityException) as e:
            self.session.rollback()
            self.logger.exception("Exception was occured.")
            raise SanicDBException(str(e))
        
        try:
            self.context.redis.unlink(user_id)
        except CommonRedisException as error:
            self.logger.exception("Exception was occured.")
            raise SanicRedisException(str(error))

        return await self.create_json_response(status=204, cookies_reset="refresh_token")