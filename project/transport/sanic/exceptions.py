from sanic.exceptions import SanicException


class SanicAuthException(SanicException):
    status_code = 401


class SanicAccessException(SanicException):
    status_code = 403


class SanicDBException(SanicException):
    status_code = 500


class SanicPasswordHashException(SanicException):
    status_code = 500


class SanicTokenException(SanicException):
    status_code = 400


class SanicRedisException(SanicException):
    status_code = 500

class SanicRequestValidationException(SanicException):
    status_code = 400


class SanicResponseValidationException(SanicException):
    status_code = 500


class SanicUserConflictException(SanicException):
    status_code = 409


class SanicUserNotFoundException(SanicException):
    status_code = 404


class SanicUserIsDeletedException(SanicException):
    status_code = 410


class SanicMessageNotFoundException(SanicException):
    status_code = 404


class SanicMessageIsDeletedException(SanicException):
    status_code = 410