from typing import Tuple

from configs.config import ApplicationConfig
from context import Context
from transport.sanic.endpoint import Endpoint
from transport.sanic.endpoints import MessageEndpoint, PingEndpoint, UserEndpoint
from transport.sanic.endpoints import AuthenticationEndpoint, DefinedMessageEndpoint


def get_routes(config: ApplicationConfig, context: Context) -> Tuple[Endpoint]:
    return (
        PingEndpoint(
            context=context, 
            uri="/", 
            methods=["GET", "PATCH", "POST"]
            ),
        UserEndpoint(
            context=context, 
            uri="/user", 
            methods=["GET", "DELETE", "PATCH"], 
            auth_required=True
            ),
        UserEndpoint(
            context=context, 
            uri="/user", 
            methods=["POST",]
            ),
        AuthenticationEndpoint(
            context=context, 
            uri="/auth", 
            methods=["GET", "POST"]
            ),
        MessageEndpoint(
            context=context, 
            uri="/msg", 
            methods=["GET", "POST"], 
            auth_required=True
            ),
        DefinedMessageEndpoint(
            context=context, 
            uri="/msg/<msg_id:int>", 
            methods=["GET", "DELETE", "PATCH"], 
            auth_required=True
            )
    )