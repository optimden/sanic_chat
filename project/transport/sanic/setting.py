from sanic import Sanic

from configs.config import ApplicationConfig
from context import Context
from hooks import init_db, init_redis, create_logger
from transport.sanic.routes import get_routes


def configure_app(config: ApplicationConfig, context: Context) -> Sanic:

    init_db(config, context)
    init_redis(config, context)
    create_logger(context)

    app = Sanic(__name__)

    for controller in get_routes(config, context):
        app.add_route(
            handler=controller,
            uri=controller.uri,
            methods=controller.methods,
            strict_slashes=True,
        )
    
    return app