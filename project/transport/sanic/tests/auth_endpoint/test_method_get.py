import unittest
from unittest.mock import patch

from .common import BaseTest
from transport.sanic.endpoints.user.auth_endpoint import AuthenticationEndpoint


class TestMethodGet(BaseTest):

    def setUp(self):
        super().setUp()
        self.endpoint = AuthenticationEndpoint(self.context, '', methods=["get",])


    @patch('transport.sanic.endpoints.user.auth_endpoint.create_token')
    @patch('transport.sanic.endpoints.user.auth_endpoint.set_refresh_token')
    @patch('transport.sanic.endpoints.user.auth_endpoint.read_token')
    async def test_return_status_200(
        self, patched_read_token, patched_set_refresh_token,
        patched_create_token
        ):
        patched_read_token.return_value = {"user_id": 1}
        patched_set_refresh_token.return_value = {
            "name": "test",
            "value": "test",
            "params": {}
        }
        patched_create_token.return_value = ""

        cookies = {"refresh_token": ""}

        request = self.request(method="get", cookies=cookies)
        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)


    async def test_refresh_token_absent(self):
        request = self.request(method="get", cookies={})
        response = await self.endpoint(request)
        self.assertEqual(response.status, 401)
    

    @patch('transport.sanic.endpoints.user.auth_endpoint.create_token')
    @patch('transport.sanic.endpoints.user.auth_endpoint.set_refresh_token')
    @patch('transport.sanic.endpoints.user.auth_endpoint.read_token')
    async def test_refresh_token_does_not_contains_user_id(
        self, patched_read_token, patched_set_refresh_token, 
        patched_create_token
        ):
        patched_read_token.return_value = {}

        cookies = {"refresh_token": ""}

        request = self.request(method="get", cookies=cookies)
        response = await self.endpoint(request)
        self.assertEqual(response.status, 400)
