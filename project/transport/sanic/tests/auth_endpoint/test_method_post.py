import unittest
from unittest.mock import patch

from .common import BaseTest
from db.database import DBSession
from transport.sanic.endpoints.user.auth_endpoint import AuthenticationEndpoint
from transport.sanic.tests.common import FakeUser


class TestMethodPost(BaseTest):

    def setUp(self):
        super().setUp()
        self.endpoint = AuthenticationEndpoint(self.context, '', methods=["post",])
    

    @patch('transport.sanic.endpoints.user.auth_endpoint.set_refresh_token')
    @patch('transport.sanic.endpoints.user.auth_endpoint.create_token')
    @patch('transport.sanic.endpoints.user.auth_endpoint.compare_hash')
    @patch.object(DBSession, 'find_user_by_login')
    async def test_request_validation(
        self, patched_find_user_by_login, patched_compare_hash,
        patched_create_token, patched_set_refresh_token
        ):
        patched_find_user_by_login.return_value = FakeUser(id=1, password="test")
        patched_compare_hash.return_value = None
        patched_create_token.return_value = ""
        patched_set_refresh_token.return_value = {
            "name": "test",
            "value": "test",
            "params": {}
        }

        payload = {
            "login": "test",
            "password": "test"
        }

        request = self.request(method="post", json=payload, content_type="application/json")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)

    
    @patch('transport.sanic.endpoints.user.auth_endpoint.set_refresh_token')
    @patch('transport.sanic.endpoints.user.auth_endpoint.create_token')
    @patch('transport.sanic.endpoints.user.auth_endpoint.compare_hash')
    @patch.object(DBSession, 'find_user_by_login')
    @patch('transport.sanic.endpoints.user.auth_endpoint.RequestAuthUserDTO')
    async def test_return_status_201(
        self, PatchedRequestAuthUserDTO, patched_find_user_by_login, 
        patched_compare_hash, patched_create_token, 
        patched_set_refresh_token
        ):
        instance = PatchedRequestAuthUserDTO.return_value

        patched_find_user_by_login.return_value = FakeUser(id=1, password="test")
        patched_compare_hash.return_value = None
        patched_create_token.return_value = ""
        patched_set_refresh_token.return_value = {
            "name": "test",
            "value": "test",
            "params": {}
        }

        payload = {
            "login": "test",
            "password": "test"
        }

        request = self.request(method="post", json=payload, content_type="application/json")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)