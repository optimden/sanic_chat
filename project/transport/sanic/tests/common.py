from unittest.mock import patch

import redis
import sqlalchemy

from db.database import DataBase, DBSession
from context import Context


class FakeUser:
    def __init__(
        self, 
        id: int = None, 
        login: str = None,
        password: str = None,
        first_name: str = None, 
        last_name: str = None,
        created_at: str = None,
        updated_at: str = None,
        is_deleted: bool = False
        ):
        self.id = id
        self.login = login
        self.password = password
        self.first_name = first_name
        self.last_name = last_name
        self.created_at = created_at
        self.updated_at = updated_at
        self.is_deleted = is_deleted


class FakeMessage:
    def __init__(
        self, 
        id: int = None, 
        created_at: str = None,
        updated_at: str = None,
        message: str = None,
        is_deleted: bool = False,
        receiver_id: int = None,
        sender_id: int = None
        ):
        self.id = id
        self.created_at = created_at
        self.updated_at = updated_at
        self.message = message
        self.is_deleted = is_deleted
        self.receiver_id = receiver_id
        self.sender_id = sender_id


def request_factory():

    class Request:

        def __init__(
            self, 
            method: str = 'get', 
            content_type: str = '',
            json: str = None,
            headers: dict = None,
            cookies: dict = None
        ):
            self.method = method.upper()
            self.content_type = content_type
            self.json = json
            self.headers = headers or {}
            self.cookies = cookies or {}
    
    return Request


@patch.object(DataBase, 'make_session')
@patch.object(sqlalchemy, 'create_engine')
def get_patched_db(patched_engine, patched_make_session) -> DataBase:
    patched_engine.return_value = None

    patched_make_session.return_value = DBSession(session=None)
    
    database = DataBase(connection=patched_engine())
    
    return database


@patch('redis.StrictRedis')
def get_patched_redis(PatchedStrictRedis) -> redis.StrictRedis:
    instance = PatchedStrictRedis.return_value
    instance.unlink.return_value = None
    instance.get.return_value = ""

    return instance


@patch('logging.Logger')
def get_patched_logger(PatchedLogger):
    instance = PatchedLogger.return_value
    instance.info.return_value = None
    instance.exception.return_value = None

    return instance



def get_patched_context(patched_db = None, patched_redis = None, patched_logger = None) -> Context:
    context = Context()

    if patched_db is not None:
        context.set('database', patched_db)

    if patched_redis is not None:
        context.set('redis', patched_redis)
    
    if patched_logger is not None:
        context.set('logger', patched_logger)

    return context