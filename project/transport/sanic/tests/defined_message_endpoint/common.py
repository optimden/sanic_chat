import aiounittest

from transport.sanic.tests.common import (
    request_factory, 
    get_patched_db,
    get_patched_logger,
    get_patched_context
)
from transport.sanic.endpoints.message.defined_message_endpoint import DefinedMessageEndpoint


class ModifiedDefinedMessageEndpoint(DefinedMessageEndpoint):
    
    async def _identify_method(self, request, body, *args, **kwargs):
        kwargs["msg_id"] = int(self.uri.split("/")[-1])
        return await super()._identify_method(request, body, *args, **kwargs)


class BaseTest(aiounittest.AsyncTestCase):

    def setUp(self):
        self.context = get_patched_context(
            patched_db=get_patched_db(), 
            patched_logger=get_patched_logger())
        self.request = request_factory()