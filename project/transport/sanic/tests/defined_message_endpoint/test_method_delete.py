import unittest
from unittest.mock import patch

from .common import BaseTest, ModifiedDefinedMessageEndpoint
from transport.sanic.endpoints.message.defined_message_endpoint import DefinedMessageEndpoint
from transport.sanic.tests.common import FakeMessage


class TestMethodDelete(BaseTest):

    def setUp(self):
        super().setUp()
        self.endpoint = ModifiedDefinedMessageEndpoint(
            self.context, '/msg/1', methods=["delete",], auth_required=True
            )


    @patch('transport.sanic.endpoints.message.defined_message_endpoint.delete_message')
    @patch.object(DefinedMessageEndpoint, 'check_auth')
    async def test_return_status_200(
        self, patched_check_auth, patched_delete_message
    ):
        patched_check_auth.return_value = {"token": {"user_id": 1}}
        patched_delete_message.return_value = FakeMessage(sender_id=1)

        request = self.request(method="delete")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)
    

    @patch('transport.sanic.endpoints.message.defined_message_endpoint.delete_message')
    @patch.object(DefinedMessageEndpoint, 'check_auth')
    async def test_user_try_delete_foreign_message(
        self, patched_check_auth, patched_delete_message
    ):
        patched_check_auth.return_value = {"token": {"user_id": 1}}
        patched_delete_message.return_value = FakeMessage(sender_id=2)

        request = self.request(method="delete")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 403)