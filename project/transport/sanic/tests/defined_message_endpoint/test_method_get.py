import unittest
from unittest.mock import patch

from .common import BaseTest, ModifiedDefinedMessageEndpoint
from db.database import DBSession
from transport.sanic.endpoints.message.defined_message_endpoint import DefinedMessageEndpoint
from transport.sanic.tests.common import FakeMessage


class TestMethodGet(BaseTest):

    def setUp(self):
        super().setUp()
        self.endpoint = ModifiedDefinedMessageEndpoint(
            self.context, '/msg/1', methods=["get",], auth_required=True
            )
    

    @patch('transport.sanic.endpoints.message.defined_message_endpoint.ResponseMessageDTO')
    @patch.object(DBSession, 'get_message')
    @patch.object(DefinedMessageEndpoint, 'check_auth')
    async def test_return_status_200(
        self, patched_check_auth, patched_get_message, PatchedResponseMessageDTO
    ):
        patched_check_auth.return_value = {"token": {"user_id": 1}}
        patched_get_message.return_value = FakeMessage(receiver_id=1, is_deleted=False)

        instance = PatchedResponseMessageDTO.return_value
        instance.dump.return_value = {}

        request = self.request(method="get")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)


    @patch.object(DBSession, 'get_message')
    @patch.object(DefinedMessageEndpoint, 'check_auth')
    async def test_response_validation(
        self, patched_check_auth, patched_get_message
    ):
        patched_check_auth.return_value = {"token": {"user_id": 1}}
        patched_get_message.return_value = FakeMessage(
            id=1, created_at="2021-01-27 12:56:08.511764",
            updated_at="2021-01-27 12:56:08.511764",
            message="test", is_deleted=False,
            receiver_id=1, sender_id=2
            )

        request = self.request(method="get")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)

    @patch('transport.sanic.endpoints.message.defined_message_endpoint.ResponseMessageDTO')
    @patch.object(DBSession, 'get_message')
    @patch.object(DefinedMessageEndpoint, 'check_auth')
    async def test_user_try_get_foreign_message(
        self, patched_check_auth, patched_get_message, PatchedResponseMessageDTO
    ):
        patched_check_auth.return_value = {"token": {"user_id": 1}}
        patched_get_message.return_value = FakeMessage(receiver_id=2, is_deleted=False)

        instance = PatchedResponseMessageDTO.return_value
        instance.dump.return_value = {}

        request = self.request(method="get")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 403)