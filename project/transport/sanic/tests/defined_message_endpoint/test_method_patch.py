import unittest
from unittest.mock import patch

from .common import BaseTest, ModifiedDefinedMessageEndpoint
from db.database import DBSession
from transport.sanic.endpoints.message.defined_message_endpoint import DefinedMessageEndpoint
from transport.sanic.tests.common import FakeMessage


class TestMethodPatch(BaseTest):

    def setUp(self):
        super().setUp()
        self.endpoint = ModifiedDefinedMessageEndpoint(
            self.context, '/msg/1', methods=["patch",], auth_required=True
            )
    

    @patch('transport.sanic.endpoints.message.defined_message_endpoint.ResponseMessageDTO')
    @patch('transport.sanic.endpoints.message.defined_message_endpoint.update_message')
    @patch('transport.sanic.endpoints.message.defined_message_endpoint.RequestUpdateMessageDTO')
    @patch.object(DefinedMessageEndpoint, 'check_auth')
    async def test_return_status_200(
        self, patched_check_auth, PatchedRequestUpdateMessageDTO,
        patched_update_message, PatchedResponseMessageDTO
    ):
        patched_check_auth.return_value = {"token": {"user_id": 1}}

        patched_request_update_message_dto = PatchedRequestUpdateMessageDTO.return_value
        patched_update_message.return_value = FakeMessage(sender_id=1)

        patched_response_message_dto = PatchedResponseMessageDTO.return_value
        patched_response_message_dto.dump.return_value = {}

        request = self.request(method="patch")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)
    

    @patch('transport.sanic.endpoints.message.defined_message_endpoint.ResponseMessageDTO')
    @patch('transport.sanic.endpoints.message.defined_message_endpoint.update_message')
    @patch.object(DefinedMessageEndpoint, 'check_auth')
    async def test_request_validation(
        self, patched_check_auth, patched_update_message, 
        PatchedResponseMessageDTO
    ):
        patched_check_auth.return_value = {"token": {"user_id": 1}}
        patched_update_message.return_value = FakeMessage(sender_id=1)

        instance = PatchedResponseMessageDTO.return_value
        instance.dump.return_value = {}

        payload = {
            "message": "test"
        }

        request = self.request(method="patch", json=payload, content_type="application/json")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)
    

    @patch('transport.sanic.endpoints.message.defined_message_endpoint.update_message')
    @patch('transport.sanic.endpoints.message.defined_message_endpoint.RequestUpdateMessageDTO')
    @patch.object(DefinedMessageEndpoint, 'check_auth')
    async def test_response_validation(
        self, patched_check_auth, PatchedRequestUpdateMessageDTO,
        patched_update_message
    ):
        patched_check_auth.return_value = {"token": {"user_id": 1}}

        instance = PatchedRequestUpdateMessageDTO.return_value

        patched_update_message.return_value = FakeMessage(
            id=1, created_at="2021-01-27 12:56:08.511764",
            updated_at="2021-01-27 12:56:08.511764",
            message="test", is_deleted=False,
            receiver_id=2, sender_id=1
        )

        request = self.request(method="patch")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)
    

    @patch('transport.sanic.endpoints.message.defined_message_endpoint.ResponseMessageDTO')
    @patch.object(DBSession, 'get_message')
    @patch('transport.sanic.endpoints.message.defined_message_endpoint.RequestUpdateMessageDTO')
    @patch.object(DefinedMessageEndpoint, 'check_auth')
    async def test_user_try_update_foreign_message(
        self, patched_check_auth, PatchedRequestUpdateMessageDTO,
        patched_get_message, PatchedResponseMessageDTO
    ):
        patched_check_auth.return_value = {"token": {"user_id": 1}}

        patched_request_update_message_dto = PatchedRequestUpdateMessageDTO.return_value
        setattr(patched_request_update_message_dto, "message", "test")

        patched_get_message.return_value = FakeMessage(
            sender_id=2, is_deleted=False
            )

        patched_response_message_dto = PatchedResponseMessageDTO.return_value
        patched_response_message_dto.dump.return_value = {}

        request = self.request(method="patch")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 403)