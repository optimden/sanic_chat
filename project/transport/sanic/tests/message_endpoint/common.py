import aiounittest

from transport.sanic.tests.common import (
    request_factory, 
    get_patched_db,
    get_patched_logger,
    get_patched_context
)


class BaseTest(aiounittest.AsyncTestCase):

    def setUp(self):
        self.context = get_patched_context(
            patched_db=get_patched_db(),
            patched_logger=get_patched_logger())
        self.request = request_factory()