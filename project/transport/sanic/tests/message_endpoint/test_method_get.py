import unittest
from unittest.mock import patch

from .common import BaseTest
from db.database import DBSession
from transport.sanic.endpoints.message.message_endpoint import MessageEndpoint


class TestMethodGet(BaseTest):

    def setUp(self):
        super().setUp()
        self.endpoint = MessageEndpoint(self.context, '', methods=['get',], auth_required=True)
    

    @patch.object(DBSession, 'get_messages')
    @patch.object(MessageEndpoint, 'check_auth')
    async def test_get_all_messages(
        self, patched_check_auth, patched_get_all_messages
        ):
        patched_check_auth.return_value = {"token": {}}
        patched_get_all_messages.return_value = []

        request = self.request(method="get")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)
