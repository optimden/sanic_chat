import unittest
from unittest.mock import patch

from .common import BaseTest
from db.database import DBSession
from transport.sanic.endpoints.message.message_endpoint import MessageEndpoint
from transport.sanic.tests.common import FakeUser


class TestMethodPost(BaseTest):

    def setUp(self):
        super().setUp()
        self.endpoint = MessageEndpoint(self.context, '', methods=["post",], auth_required=True)
    

    @patch('transport.sanic.endpoints.message.message_endpoint.create_message')
    @patch.object(DBSession, 'find_user_by_login')
    @patch.object(DBSession, 'find_user_by_id')
    @patch.object(DBSession, 'get_messages')
    @patch.object(MessageEndpoint, 'check_auth')
    async def test_get_all_messages(
        self, patched_check_auth, patched_get_all_messages,
        patched_find_user_by_id, patched_find_user_by_login,
        patched_create_message
        ):
        patched_check_auth.return_value = {"token": {}}
        patched_find_user_by_id.return_value = None
        patched_find_user_by_login.return_value = FakeUser(id=1)
        patched_create_message.return_value = None

        payload = {
            "message": "test",
            "recipient": "test"
        }

        request = self.request(method="post", json=payload, content_type='application/json')

        response = await self.endpoint(request)
        self.assertEqual(response.status, 201)