from datetime import datetime
import json
import unittest
from unittest.mock import patch

import aiounittest

from transport.sanic.tests.common import (
    request_factory, 
    get_patched_db,
    get_patched_logger,
    get_patched_context
)
from transport.sanic.endpoints.base import BaseEndpoint


class TestBaseEndpoint(aiounittest.AsyncTestCase):

    def setUp(self):
        self.context = get_patched_context(
            patched_db=get_patched_db(),
            patched_logger=get_patched_logger()
            )
        self.request = request_factory()
        self.endpoint = BaseEndpoint(self.context, '', ["get",], True)

    async def test_unauthorized_access_resolving(self):

        request = self.request(method="get")

        response = await self.endpoint(request)

        self.assertEqual(response.status, 401)

    @patch.object(BaseEndpoint, 'check_auth')
    async def test_method_resolving(self, patched_check_auth):

        patched_check_auth.return_value = {"token": {}}

        request = self.request(method="get")

        response = await self.endpoint(request)

        self.assertEqual(response.status, 501)