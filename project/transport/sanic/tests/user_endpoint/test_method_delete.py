import unittest
from unittest.mock import patch

from .common import BaseTest
from transport.sanic.endpoints.user.user_endpoint import UserEndpoint
from transport.sanic.tests.common import FakeUser


class TestMethodDelete(BaseTest):

    def setUp(self):
        super().setUp()
        self.endpoint = UserEndpoint(self.context, '', methods=["delete",], auth_required=True)


    @patch.object(UserEndpoint, 'check_auth')
    @patch('transport.sanic.endpoints.user.user_endpoint.delete_user')
    async def test_method_delete_return_204_status(
        self, patched_delete_user, patched_check_auth
        ):
        patched_check_auth.return_value = {"token": {}}
        patched_delete_user.return_value = None

        request = self.request(method="delete")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 204)


        