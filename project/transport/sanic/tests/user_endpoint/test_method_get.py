import unittest
from unittest.mock import patch

from .common import BaseTest
from db.database import DBSession
from transport.sanic.tests.common import FakeUser
from transport.sanic.endpoints.user.user_endpoint import UserEndpoint


class TestMethodGet(BaseTest):

    def setUp(self):
        super().setUp()
        self.endpoint = UserEndpoint(self.context, '', ["get",], auth_required=True)


    @patch.object(UserEndpoint, 'check_auth')
    @patch.object(DBSession, 'find_user_by_id')
    async def test_response_validation(
        self, patched_find_user_by_id, patched_check_auth
        ):
        patched_check_auth.return_value = {"token": {}}
        patched_find_user_by_id.return_value = FakeUser(
            id=1, login="test", first_name="test", last_name="test", 
            created_at="2021-01-27 12:56:08.511764",
            updated_at="2021-01-27 12:56:08.511764"
            )

        request = self.request(method="get")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)