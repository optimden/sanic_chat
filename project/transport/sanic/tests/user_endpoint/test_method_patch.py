from datetime import datetime
import unittest
from unittest.mock import patch

from .common import BaseTest
from transport.sanic.tests.common import FakeUser
from transport.sanic.endpoints.user.user_endpoint import UserEndpoint


class TestMethodPatch(BaseTest):

    def setUp(self):
        super().setUp()
        self.endpoint = UserEndpoint(self.context, '', ["patch",], auth_required=True)
    

    @patch('transport.sanic.endpoints.user.user_endpoint.ResponseUserDTO')
    @patch.object(UserEndpoint, 'check_auth')
    @patch('transport.sanic.endpoints.user.user_endpoint.update_user')
    async def test_method_patch_request_validation(
        self, patched_update_user, patched_check_auth, PatchedResponseUserDTO
        ):
        patched_check_auth.return_value = {"token": {}}
        patched_update_user.return_value = None

        instance = PatchedResponseUserDTO.return_value
        instance.dump.return_value = {}

        payload = {
            "first_name": "test",
            "last_name": "test"
            }

        request = self.request(method="patch", json=payload, content_type='application/json')

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)


    @patch('transport.sanic.endpoints.user.user_endpoint.RequestUpdateUserDTO')
    @patch.object(UserEndpoint, 'check_auth')
    @patch('transport.sanic.endpoints.user.user_endpoint.update_user')
    async def test_method_patch_response_validation(
        self, patched_update_user, patched_check_auth, PatchedRequestUpdateUserDTO
        ):
        patched_check_auth.return_value = {"token": {}}
        patched_update_user.return_value = FakeUser(
            id=1, login="test", first_name="test", last_name="test", 
            created_at="2021-01-27 12:56:08.511764",
            updated_at="2021-01-27 12:56:08.511764"
            )
        instance = PatchedRequestUpdateUserDTO.return_value

        request = self.request(method="patch")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 200)