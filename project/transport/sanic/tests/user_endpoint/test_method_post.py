import unittest
from unittest.mock import patch

from .common import BaseTest
from transport.sanic.tests.common import FakeUser
from transport.sanic.endpoints.user.user_endpoint import UserEndpoint


class TestMethodPost(BaseTest):

    def setUp(self):
        super().setUp()
        self.endpoint = UserEndpoint(self.context, '', ["post",])
    

    @patch('transport.sanic.endpoints.user.user_endpoint.set_refresh_token')
    @patch('transport.sanic.endpoints.user.user_endpoint.create_token')
    @patch('transport.sanic.endpoints.user.user_endpoint.ResponseUserDTO')
    @patch('transport.sanic.endpoints.user.user_endpoint.create_user')
    async def test_method_post_request_validation(
        self, patched_create_user, PatchedResponseUserDTO, 
        patched_create_token, patched_set_refresh_token
        ):
        patched_create_user.return_value = FakeUser(id=1)

        instance = PatchedResponseUserDTO.return_value
        instance.dump.return_value = {}

        patched_create_token.return_value = ''
        patched_set_refresh_token.return_value = {
            "name": "test",
            "value": "test",
            "params": {}
        }

        payload = {
            "login": "test",
            "password": "test",
            "first_name": "test",
            "last_name": "test"
            }

        request = self.request(method="post", json=payload, content_type='application/json')

        response = await self.endpoint(request)
        self.assertEqual(response.status, 201)


    @patch('transport.sanic.endpoints.user.user_endpoint.generate_hash')
    @patch('transport.sanic.endpoints.user.user_endpoint.set_refresh_token')
    @patch('transport.sanic.endpoints.user.user_endpoint.create_token')
    @patch('transport.sanic.endpoints.user.user_endpoint.RequestCreateUserDTO')
    @patch('transport.sanic.endpoints.user.user_endpoint.create_user')
    async def test_method_post_response_validation(
        self, patched_create_user, PatchedRequestCreateUserDTO,
        patched_create_token, patched_set_refresh_token,
        patched_generate_hash
        ):
        patched_create_user.return_value = FakeUser(
            id=1, login="test", first_name="test", last_name="test", 
            created_at="2021-01-27 12:56:08.511764",
            updated_at="2021-01-27 12:56:08.511764"
            )

        instance = PatchedRequestCreateUserDTO.return_value

        patched_create_token.return_value = ''
        patched_set_refresh_token.return_value = {
            "name": "test",
            "value": "test",
            "params": {}
        }
        patched_generate_hash.return_value = None

        request = self.request(method="post")

        response = await self.endpoint(request)
        self.assertEqual(response.status, 201)