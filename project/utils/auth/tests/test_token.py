import unittest

from utils.auth.token import create_token, read_token
from utils.auth.exceptions import DecodeTokenException


class TestToken(unittest.TestCase):

    def setUp(self):
        self.payload = {
            "user_id": 1,
        }
        

    def test_previously_generated_token(self):
        created_token = create_token(self.payload, 1)
        decoded_payload = read_token(created_token)

        cleaned_payload = {
            key: value
            for key, value in decoded_payload.items()
            if (key, value) in self.payload.items()
        }

        self.assertEqual(self.payload, cleaned_payload)
    

    def test_handling_expired_token(self):
        created_token = create_token(self.payload, -7)

        with self.assertRaises(DecodeTokenException):
            decoded_token = read_token(created_token)
