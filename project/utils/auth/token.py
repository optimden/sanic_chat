import datetime
import os

from dotenv import load_dotenv
import jwt 

from .exceptions import DecodeTokenException
from db.exceptions import CommonRedisException
from db.redis import Redis


load_dotenv()

secret = os.getenv("JWT_PRIVATE_KEY")

def create_token(payload: dict, lifetime: int) -> str:
    data = {
        "exp": datetime.datetime.utcnow() + datetime.timedelta(days=lifetime)
    }
    data.update(payload)
    return jwt.encode(data, secret, algorithm="HS256")
    

def read_token(token: str) -> dict:
    try:
        return jwt.decode(token, secret, algorithms=["HS256",])
    except jwt.exceptions.PyJWTError:
        raise DecodeTokenException


def set_refresh_token(redis: Redis, user_id: int, *, lifetime: int) -> dict:
    """ Генерирует refresh токен, сохраняет его в Redis и добавляет его в словарь,
    который потом будет установлен в cookies. """
    refresh = create_token({"user_id": user_id, "type": "refresh"}, lifetime=lifetime)

    redis.set_(user_id, refresh)

    return {
        "name": "refresh_token",
        "value": refresh,
        "params": {
            "expires": datetime.datetime.utcnow() + datetime.timedelta(days=lifetime),
            "httponly": True,
        }
    }