class GeneratePasswordHashException(Exception):
    pass


class CheckPasswordHashException(Exception):
    pass