import bcrypt

from .exceptions import CheckPasswordHashException, GeneratePasswordHashException


def generate_hash(password: str) -> str:
    try:
        return bcrypt.hashpw(
            password=bytes(password, encoding="utf-8"),
            salt=bcrypt.gensalt(),
        )
    except (TypeError, ValueError) as error:
        raise GeneratePasswordHashException(str(error))


def compare_hash(password: str, hash: bytes):
    try:
        result = bcrypt.checkpw(
            password=bytes(password, encoding="utf-8"),
            hashed_password=hash
        )
    except (TypeError, ValueError) as error:
        raise CheckPasswordHashException(str(error))

    if not result:
        raise CheckPasswordHashException
