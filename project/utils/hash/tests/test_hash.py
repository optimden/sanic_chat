import unittest

from .hash import generate_hash, compare_hash
from .exceptions import GeneratePasswordHashException, CheckPasswordHashException


class TestHash(unittest.TestCase):

    def setUp(self):
        self.password = "test_password123"


    def test_comparing_hash(self):
        hash = generate_hash(self.password)
        try:
            compare_hash(self.password, hash)
        except Exception as error:
            self.fail("Exception was occured during comparing hash.")


    def test_wrong_hashed_variable_type(self):
        """ Тестируем корректность обработки неправильного типа
        входного значения, из которого делается хэш. """

        bytes_password = self.password.encode("utf-8")

        with self.assertRaises(GeneratePasswordHashException):
            hash = generate_hash(bytes_password)
    

    def test_wrong_variable_type_comparing_with_hash(self):
        """ Тестируем корректность обработки неправильного типа 
        входного значения, хэш которого сравнивается с имеющимся
        хэшем. """

        bytes_password = self.password.encode("utf-8")

        hash = generate_hash(self.password)

        with self.assertRaises(CheckPasswordHashException):
            compare_hash(bytes_password, hash)
    

    def test_wrong_hash_type_comparing_with_string_password(self):
        """ Тестируем корректность обработки неправильного типа 
        входного значения, хэш которого сравнивается с имеющимся
        хэшем. """

        hash = str(generate_hash(self.password))

        with self.assertRaises(CheckPasswordHashException):
            compare_hash(self.password, hash)



        

